>>>> Please feel free to suggest improvements and additions to this file! <<<<

BLOCK EXPIRE MODULE FOR DRUPAL 7.x
---------------------------

CONTENTS OF THIS README
-----------------------

   * Description
   * Requirements
   * Installation & Configuration
   * Credits

DESCRIPTION
-----------

This module help to expire static cached page of 
block display listing pages/urls. This is currently 
support boost static cached pages. This is tested with Boost only.

REQUIREMENTS
------------
 - Cache Expiration   https://www.drupal.org/project/expire
 - Boost (Optional)   https://www.drupal.org/project/boost


Installation & Configuration
----------------------------

1. Goto: [Administer > Configuration > System > Cache Expiration > Block expiration] 
and  and review the default settings for block action.
2. Default settings can override on each module configuration 
under Block expiration settings


CREDITS
-------
7.x Originally developed by Rajesh Saini.
7.x Developed & Maintained by
    Rajesh Saini https://www.drupal.org/user/1347076
