<?php

/**
 * @file
 * Block caching Mechanism for expire module
 */

/**
 * Hook_form_FORM_ID_alter
 * Extent block cache expire setting on indivisual block
 */
function block_expire_form_block_admin_display_form_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = 'block_display_cache_flushed';
}

/**
 * Flush all cache at submit handler for display setting to block.
 */
function block_display_cache_flushed($form, $form_state) {
  if (variable_get("block_expire_default_block_management")) {
    register_shutdown_function('_block_expire_flush_all_cached_pages');
  }
}

/**
 * Hook_form_FORM_ID_alter
 * Extent block cache expire setting
 */
function block_expire_form_expire_admin_settings_form_alter(&$form, &$form_state, $form_id) {
  //p($form['tabs']);
  $form['tabs']['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block expiration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#weight' => 10,
  );

  $form['tabs']['block']['block_expire'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block Action'),
  );

  // NODE SETTINGS.
  $form['tabs']['block']['block_expire']['block_expire_default_block_management'] = array(
    '#type' => 'checkbox',
    '#title' => t('Block Management Update'),
    '#default_value' => variable_get('block_expire_default_block_management'),
    '#description' => t('Expire all cached pages after selected actions.(admin/structure/block).'),
  );
  $form['tabs']['block']['block_expire']['block_expire_default_individual_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Individual block Update'),
    '#default_value' => variable_get('block_expire_default_individual_block'),
    '#description' => t('Page cache for block display will be flushed after selected actions.'),
  );
  $form['#submit'][] = 'block_expire_default_configure_submit';
}

function block_expire_default_configure_submit($form, $form_state) {
  variable_set("block_expire_default_block_management", $form_state['values']['block_expire_default_block_management']);
  variable_set("block_expire_default_individual_block", $form_state['values']['block_expire_default_individual_block']);
}

/**
 * Hook_form_FORM_ID_alter
 * Extent block cache expire setting on indivisual block
 */
function block_expire_form_block_admin_configure_alter(&$form, &$form_state, $form_id) {

  if (variable_get("block_expire_default_individual_block", 0)) {
    $form['visibility']['block_expire'] = array(
      '#type' => 'fieldset',
      '#title' => t('Block expiration'),
      '#weight' => 10,
    );

    $form['visibility']['block_expire']['block_caching_override'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override default settings for this node type'),
      '#default_value' => variable_get("block_expire_caching_override_" . $form['delta']['#value']),
    );
    $form['visibility']['block_expire']['block_caching'] = array(
      '#type' => 'fieldset',
      '#title' => t('Block display realted page(s) cache expired'),
    );
    $form['visibility']['block_expire']['block_caching']['#states'] = array(
      'visible' => array(':input[name="block_caching_override"]' => array('checked' => TRUE)),
    );
    $form['visibility']['block_expire']['block_caching']['block_expire_setting'] = array(
      '#type' => 'checkboxes',
      '#description' => t('Block display pages cache will be flushed after selected actions.'),
      '#options' => array(
        3 => t('Front page expire'),
        2 => t('Block configuration saved action'),
        1 => t('Node insert, update & delete action'),
      ),
      '#default_value' => variable_get("block_expire_setting_" . $form['delta']['#value'], array()),
    );

    $form['visibility']['block_expire']['on_node_type'] = array(
      '#type' => 'fieldset',
      '#title' => t('Block Content Type Relation'),
    );
    $form['visibility']['block_expire']['on_node_type']['#states'] = array(
      'visible' => array(':input[name="block_expire_setting[1]"]' => array('checked' => TRUE)),
    );
    $form['visibility']['block_expire']['on_node_type']['block_content_relation'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select block content has relation with specific content type(s)'),
      '#options' => node_type_get_names(),
      '#default_value' => variable_get("block_expire_content_relation_" . $form['delta']['#value'], array()),
      '#description' => t('Node action will be releated on selected content type(s). If you select no types, there will be no type-specific limitation.'),
    );

    $form['#submit'][] = 'block_cahcing_admin_configure_submit';
    $form['#submit'][] = 'block_caching_flush_cached';
    // Attach js.
    $form['#attached']['js'][] = drupal_get_path('module', 'block_expire') . '/js/block.expire.js';
  }
}

/**
 * Admin configuration setting submit
 */
function block_cahcing_admin_configure_submit($form, $form_state) {
  $page = '';
  $theme_default = variable_get('theme_default', 'bartik');
  if ($form_state['values']['visibility'] == 1) {
    $page_list = explode(PHP_EOL, $form_state['values']['pages']);
    $page = implode(',', $page_list);
  }
  db_delete('block_cache_expaire')->condition('delta', trim($form_state['values']['delta']), 'LIKE')->execute();
  if (!empty($form_state['values']['block_caching_override']) && ($form_state['values']['regions'][$theme_default] != '-1')) {
    if (!empty($form_state['values']['block_expire_setting'][1]) || !empty($form_state['values']['block_expire_setting'][2])) {
      $query_data = array();
      if (!empty($form_state['values']['block_content_relation'])) {
        foreach ($form_state['values']['block_content_relation'] as $node_type => $status) {
          if ($status) {
            $block_cache_query = array();
            $block_cache_query['module'] = $form_state['values']['module'];
            $block_cache_query['delta'] = $form_state['values']['delta'];
            $block_cache_query['page'] = !empty($page) ? $page : 'all';

            $block_cache_query['node_type'] = $node_type;
            $block_cache_query['node_type_status'] = !empty($status) ? 1 : 0;
            $query_data[] = $block_cache_query;
            if (!empty($form_state['values']['block_expire_setting'][3])) {
              $block_cache_query['page'] = '<front>';
              $query_data[] = $block_cache_query;
            }
          }
        }
      }
      if (!empty($query_data)) {
        foreach ($query_data as $data) {
          $nid = db_insert('block_cache_expaire')->fields(array(
              'module' => $data['module'],
              'delta' => $data['delta'],
              'node_type' => $data['node_type'],
              'pages' => !empty($page) ? trim($page) : 'all',
            ))->execute();
        }
      }
      else {
        $nid = db_insert('block_cache_expaire')->fields(array(
            'module' => $form_state['values']['module'],
            'delta' => $form_state['values']['delta'],
            'node_type' => 'all',
            'pages' => !empty($page) ? $page : 'all',
          ))->execute();
      }
    }
  }
  if ($form_state['values']['block_caching_override']) {
    variable_set("block_expire_content_relation_" . $form['delta']['#value'], $form_state['values']['block_content_relation']);
    variable_set('block_expire_setting_' . $form['delta']['#value'], $form_state['values']['block_expire_setting']);
    variable_set("block_expire_caching_override_" . $form['delta']['#value'], $form_state['values']['block_caching_override']);
  }
  else {
    variable_del("block_expire_content_relation_" . $form['delta']['#value']);
    variable_del('block_expire_setting_' . $form['delta']['#value']);
    variable_del("block_expire_caching_override_" . $form['delta']['#value']);
  }
}

/**
 * Flush individual block display pages cached.
 */
function block_caching_flush_cached($form, $form_state) {
  $expire_urls = array();
  module_load_include('inc', 'expire', 'include/expire.api');
  if (empty($form_state['values']['block_caching_override'])) {
    if (variable_get("block_expire_default_individual_block", 0)) {
      $expire_urls = block_default_cached_flush($form_state);
      if (!empty($expire_urls)) {
        ExpireAPI::executeExpiration($expire_urls);
      }
      return;
    }
    else {
      return;
    }
  }

  if (!empty($form_state['values']['block_expire_setting'][2])) {
    global $base_url;
    $expire_urls = array();
    $limited_on_type = 0;
    if (!empty($form_state['values']['type'])) {
      foreach ($form_state['values']['type'] as $type => $checked) {
        if ($checked) {
          $limited_on_type++;
        }
      }
    }

    if ($form_state['values']['visibility'] == 1) {
      $block_display_urls = explode(PHP_EOL, $form_state['values']['pages']);

      foreach ($block_display_urls as $block_display_url) {
        if ($block_display_url == '<front>') {
          $expire_urls += _block_expire_front_page_url();
        }
        else {
          $block_display_url = trim($block_display_url);
          $expire_urls[$block_display_url] = "$base_url/$block_display_url";
        }
      }
    }
    else {

      if (!empty($form_state['values']['block_content_relation'])) {
        $node_type_list = array();
        foreach ($form_state['values']['block_content_relation'] as $node_type => $status) {
          if ($status) {
            $node_type_list[] = trim($node_type);
          }
        }

        if ($form_state['values']['visibility'] == 0 && $limited_on_type == 0 && empty($node_type_list)) {
          _block_expire_flush_all_cached_pages();
          return;
        }

        if (!empty($node_type_list)) {
          $expire_urls = node_type_url_list(array($node_type_list));
        }
      }
      else {
        $expire_urls = node_type_url_list();
      }
    }
    if (!empty($form_state['values']['block_expire_setting'][3])) {
      $expire_urls += _block_expire_front_page_url();
    }
    if (!empty($expire_urls)) {
      ExpireAPI::executeExpiration($expire_urls);
    }
  }
}

/**
 * @method type block_default_cached_flush(array $form_state)
 *
 * @param $form_state
 *   form_state type array.
 *
 * @return array
 *   List of urls for expire.
 */
function block_default_cached_flush($form_state) {
  $expire_urls = array();
  if ($form_state['values']['visibility'] == 1) {
    $block_display_urls = explode(PHP_EOL, $form_state['values']['pages']);

    foreach ($block_display_urls as $block_display_url) {
      if ($block_display_url == '<front>') {
        $expire_urls += _block_expire_front_page_url();
      }
      else {
        $block_display_url = trim($block_display_url);
        $expire_urls[$block_display_url] = "$base_url/$block_display_url";
      }
    }
  }
  else {
    if (!empty($form_state['values']['type'])) {
      $node_type_list = array();
      foreach ($form_state['values']['block_content_relation'] as $node_type => $checked) {
        if ($checked) {
          $node_type_list[] = trim($node_type);
        }
      }

      if ($form_state['values']['visibility'] == 0 && empty($node_type_list)) {
        // Allow other modules to modify the list prior to expiring.
        // drupal_alter('_block_expire_flush_all_cached_pages', $override = FALSE);
        _block_expire_flush_all_cached_pages();
        return;
      }
      if (!empty($node_type_list)) {
        $expire_urls = node_type_url_list(array($node_type_list));
      }
    }
    else {
      $expire_urls = node_type_url_list();
    }
  }
  return $expire_urls;
}

/**
 * @method type node_type_url_list(array $type)
 *
 * @param $type
 *   Node type array.
 *
 * @return array
 *   List of urls.
 */
function node_type_url_list($type = array()) {
  global $base_url;
  $node_url = array();
  if (empty($type)) {
    $type = node_type_get_names();
  }
  $node_type = array_keys($type);
  $query = db_select('node', 'n')->fields('n', array('nid', 'type'))->condition('type', $type, 'IN')->condition('status', 1)->orderBy('nid', 'ASC');
  $result = $query->execute();
  while ($record = $result->fetchAssoc()) {
    $nid = $record['nid'];
    $internal_path = drupal_get_path_alias("node/$nid");
    $node_url[$internal_path] = $base_url . '/' . $internal_path;
  }
  return $node_url;
}

/**
 *  flushed all cached for boost module
 */
function _block_expire_flush_all_cached_pages() {
  if (module_exists('boost')) {
    $url_info = boost_parse_url();
    $url_info['host'];
    $cached_dir = boost_get_normal_cache_dir();
    $boost_cached_dir = $cached_dir . '/' . $url_info['host'];
    if ($boost_cached_dir && is_dir($boost_cached_dir)) {
      if (_boost_rmdir($boost_cached_dir)) {
        watchdog('Block Expire', 'Flushed boost\'s cached pages', array(), WATCHDOG_DEBUG);
      }
    }
  }
}

/**
 * Return home page urls
 */
function _block_expire_front_page_url() {
  $list = array();
  $front_url = array();
  $site_url = url('<front>', array('absolute' => TRUE));
  $front_url['front'] = '';
  $front_url['front-path'] = variable_get('site_frontpage', 'node');
  foreach ($front_url as $furl) {
    $list[$furl] = $site_url . $furl;
  }
  return $list;
}

