/**
 * @file
 * IWD Caching javascript code
 */
(function ($) {

  Drupal.behaviors.blockExpire = {
    attach: function (context, settings) {

      $('fieldset#edit-block-expire', context).drupalSetSummary(function(context) {
        var vals = [];
        if ($('#edit-block-caching-override', context).is(':checked')) {
          vals.push(Drupal.t('Block display expiration: settings are overriden'));
        }
        else {
          vals.push(Drupal.t('Block display expiration: default settings'));
        }
        return vals.join(', ');
      });
    }
  };
  
})(jQuery);
